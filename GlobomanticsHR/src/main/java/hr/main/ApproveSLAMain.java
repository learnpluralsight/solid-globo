package hr.main;

import hr.logging.ConsoleLogger;
import hr.persistence.EmployeeFileSerializer;
import hr.persistence.EmployeeRepository;
import hr.personnel.Employee;
import hr.personnel.ServiceLicenseAgreement;
import hr.personnel.Subcontractor;

import java.util.Arrays;
import java.util.List;

public class ApproveSLAMain {
    public static void main(String[] args) {
        // Create dependencies
        ConsoleLogger consoleLogger = new ConsoleLogger();

        int minTimeOffPercent = 98;
        int maxResolutionDays = 2;

        ServiceLicenseAgreement companySla = new ServiceLicenseAgreement(
                minTimeOffPercent,
                maxResolutionDays);

        Subcontractor subcontractor1 = new Subcontractor(
                "Rebekah Jackson",
                "rebekah-jackson@abc.com",
                3000,
                15
        );

        Subcontractor subcontractor2 = new Subcontractor(
                "Harris Fitz",
                "rebekah-jackson@def.com",
                3000,
                15
        );

        // Grab subcontractors
        List<Subcontractor> collaborators = Arrays.asList(subcontractor1, subcontractor2);

        for (Subcontractor s : collaborators){
            s.approveSLA(companySla);
        }
    }
}
