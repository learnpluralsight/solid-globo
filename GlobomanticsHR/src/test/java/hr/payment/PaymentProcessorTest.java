package hr.payment;

import hr.notifications.EmployeeNotifier;
import hr.persistence.EmployeeRepositoryNew;
import hr.personnel.Employee;
import hr.personnel.FullTimeEmployee;
import hr.personnel.Intern;
import hr.personnel.PartTimeEmployee;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PaymentProcessorTest {

    private EmployeeRepositoryNew employeeRepositoryMock;
    private EmployeeNotifier employeeNotifierMock;

    @BeforeAll
    public void beforeAll(){
        List<Employee> testEmployee = Arrays.asList(
                new FullTimeEmployee("Anna Smith", 1000),
                new PartTimeEmployee("Anna Smith", 500),
                new Intern("Anna Smith", 200,100)
        );

        employeeRepositoryMock = Mockito.mock(EmployeeRepositoryNew.class);

        Mockito.when(employeeRepositoryMock.findAll())
                .thenReturn(testEmployee);

        employeeNotifierMock = Mockito.mock(EmployeeNotifier.class);
    }


    @Test
    public void send_payments_should_pay_all_employee_salaries(){
         // arrange
        PaymentProcessor paymentProcessor = new PaymentProcessor(
                this.employeeRepositoryMock,
                this.employeeNotifierMock
        );

        // act
        int result = paymentProcessor.sendPayments();

        // assert
        assertEquals(1700, result);
    }
}